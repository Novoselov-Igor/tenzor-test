<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductRequest extends Model
{
    use HasFactory;

    protected $table = 'product_requests';

    protected $fillable = [
        'client_fio',
        'product_id',
        'telephone',
        'product_count',
        'sum'
    ];


}
