<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\ProductRequest;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class ProductsController extends Controller
{
    public function getPage()
    {
        $products = Product::all();
        return view('mainPage', compact('products'));
    }

    public function sendData(Request $request)
    {
        try {
            $validated = $request->validate([
                'client_fio' => 'required',
                'product_id' => 'required',
                'telephone' => 'required',
                'product_count' => 'required|min:1',
                'sum'=> ''
            ]);
            ProductRequest::create($validated);
            return redirect('/')->with('success','Заявка успешно создана');

        } catch (\Exception $exception)
        {
            return redirect('/')->withErrors($exception);
        }

    }
}
