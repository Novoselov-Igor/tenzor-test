<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Заявки</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

    <h1 style="text-align: center"> Заявки на покупку товара</h1>
</head>
<body>
<form class="container" method="post" name="form1">
    @csrf
    <div class="input-group mb-3" style="align-items: center">
        <span class="input-group-text">ФИО</span>
        <input type="text" class="form-control" name="client_fio" required>
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text">Товар</span>
        <select name="product_id" class="form-control" id="product">
            <option disabled selected>Выберите товар</option>
            @foreach($products as $product)
                <option value="{{ $product->id }}">{{ $product->name }}(Цена: {{ $product->price }})</option>
            @endforeach
        </select>
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text">Телефон</span>
        <input type="text" class="form-control" name="telephone" required>
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text">Количество товара</span>
        <input min="1" type="number" class="form-control" name="product_count" id="product_count" required onchange="calculateSum()">
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text">Сумма</span>
        <input type="number" class="form-control" name="sum" id="sum" value="0" readonly>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@if(Session::has('success'))
    <div class="alert alert-success">
        <p>{{Session::get('success')}}</p>
    </div>
@elseif($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
</body>
<script>
    function calculateSum() {
        var a = document.getElementById('product').options[document.getElementById('product').value].text;
        a = a.replace(/[a-zа-яё:() ]/gi, '');
        document.getElementById('sum').value = a * document.getElementById('product_count').value;
    }
</script>

</html>
