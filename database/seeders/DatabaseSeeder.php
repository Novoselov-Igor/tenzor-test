<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->insert([
            ['category' => 'Ноутбуки'],
            ['category' => 'Компьютеры и моноблоки'],
            ['category' => 'Комплектующие ПК'],
        ]);
        DB::table('products')->insert([
            ['name' => 'Huawei Matebook ','product_category_id' => '1', 'price' => '103990'],
            ['name' => 'Honor MagicBook','product_category_id' => '1', 'price' => '54990'],
            ['name' => 'Apple IMac ','product_category_id' => '2', 'price' => '151990'],
            ['name' => 'TECLAST ','product_category_id' => '1', 'price' => '68990'],
            ['name' => 'Материнская плата ','product_category_id' => '3', 'price' => '6000'],
            ['name' => 'Процессор ','product_category_id' => '3', 'price' => '15000'],
        ]);
    }
}
